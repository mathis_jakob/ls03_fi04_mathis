package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import java.awt.Button;
import javax.swing.JTextField;
import java.awt.SystemColor;
import java.awt.Choice;

public class MyGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;
	private JLabel lblDieserTextSoll;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGUI frame = new MyGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 432, 654);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnRot = new JButton("Rot");
		btnRot.setBounds(10, 122, 121, 23);
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRot_clicked();			
			}
		});
		contentPane.setLayout(null);
		btnRot.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnRot);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.setBounds(141, 122, 129, 23);
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGrn_clicked();
			}
		});
		btnGrn.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.setBounds(282, 122, 121, 23);
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlau_clicked();
			}
		});
		btnBlau.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.setBounds(10, 156, 121, 23);
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGelb_clicked();
			}
		});
		btnGelb.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.setBounds(141, 156, 129, 23);
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonStandard_clicked();
			}
		});
		btnStandardfarbe.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.setBounds(282, 156, 121, 23);
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonWaehlen_clicked();
			}
		});
		btnFarbeWhlen.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnFarbeWhlen);
		
		JButton btnArial = new JButton("Arial");
		btnArial.setBounds(10, 221, 121, 23);
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonArial_clicked();
			}
		});
		btnArial.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.setBounds(141, 221, 129, 23);
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonComic_clicked();
			}
		});
		btnComicSansMs.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnComicSansMs);
		
		JButton btnNew = new JButton("Courier New");
		btnNew.setBounds(282, 220, 121, 23);
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCourier_clicked();
			}
		});
		btnNew.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnNew);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.setBounds(10, 286, 188, 23);
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonTexteing_clicked();
			}
		});
		btnInsLabelSchreiben.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.setBounds(218, 286, 185, 23);
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonLoeschen_clicked();
			}
		});
		btnTextImLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnTextImLabel);
		
		JButton btnRot_11 = new JButton("Rot");
		btnRot_11.setBounds(10, 351, 121, 23);
		btnRot_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonSchriftfarberot_clicked();
			}
		});
		btnRot_11.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnRot_11);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.setBounds(141, 351, 129, 23);
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonSchriftfarbeblau_clicked();
			}
		});
		btnBlau_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.setBounds(282, 350, 121, 23);
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonSchriftfarbeschwarz_clicked();
			}
		});
		btnSchwarz.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnSchwarz);
		
		JButton btnPlus = new JButton("+");
		btnPlus.setBounds(10, 416, 188, 23);
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGroesser_clicked();
			}
		});
		btnPlus.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnPlus);
		
		JButton btnMinus = new JButton("-");
		btnMinus.setBounds(218, 416, 185, 23);
		btnMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonKleiner_clicked();
			}
		});
		btnMinus.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnMinus);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setBounds(10, 255, 383, 20);
		txtHierBitteText.setText("Hier bitte Text eingeben");
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.setBounds(10, 481, 121, 23);
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonLinks_clicked();
			}
		});
		btnLinksbndig.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.setBounds(141, 481, 129, 23);
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonZentriert_clicked();
			}
		});
		btnZentriert.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnZentriert);
		
		JButton btnRechtbndig = new JButton("rechtb\u00FCndig");
		btnRechtbndig.setBounds(282, 481, 121, 23);
		btnRechtbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRechts_clicked();
			}
		});
		btnRechtbndig.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnRechtbndig);
		
		JButton btnExit = new JButton("Exit");
		btnExit.setBounds(10, 546, 393, 58);
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonExit_clicked();
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(btnExit);
		
		lblDieserTextSoll = new JLabel("Dieser Text soll ver\u00E4ndert werden");
		lblDieserTextSoll.setBounds(10, 11, 393, 69);
		lblDieserTextSoll.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDieserTextSoll.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblDieserTextSoll);
		
		JLabel lblAufgabehintergrund = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabehintergrund.setBounds(10, 97, 250, 14);
		lblAufgabehintergrund.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(lblAufgabehintergrund);
		
		JLabel lblAufgabeText = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabeText.setBounds(10, 196, 250, 14);
		lblAufgabeText.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(lblAufgabeText);
		
		JLabel lblAufgabeschriftfarbendern = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabeschriftfarbendern.setBounds(10, 326, 250, 14);
		lblAufgabeschriftfarbendern.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(lblAufgabeschriftfarbendern);
		
		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabeSchriftgre.setBounds(10, 391, 250, 14);
		lblAufgabeSchriftgre.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(lblAufgabeSchriftgre);
		
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setBounds(10, 456, 250, 14);
		lblAufgabeTextausrichtung.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(lblAufgabeTextausrichtung);
		
		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm.setBounds(10, 521, 250, 14);
		lblAufgabeProgramm.setFont(new Font("Tahoma", Font.BOLD, 12));
		contentPane.add(lblAufgabeProgramm);
	}

	public void buttonWaehlen_clicked() {
		this.contentPane.setBackground(JColorChooser.showDialog(contentPane, "W�hle Farbe", Color.WHITE));
		
	}

	public void buttonExit_clicked() {
		System.exit(1);
		
	}

	public void buttonRechts_clicked() {
		lblDieserTextSoll.setHorizontalAlignment(SwingConstants.RIGHT);
		
	}

	public void buttonZentriert_clicked() {
		lblDieserTextSoll.setHorizontalAlignment(SwingConstants.CENTER);
		
	}

	public void buttonLinks_clicked() {
		lblDieserTextSoll.setHorizontalAlignment(SwingConstants.LEFT);
		
	}

	public void buttonKleiner_clicked() {
		int groesse = lblDieserTextSoll.getFont().getSize();
		String schriftart = lblDieserTextSoll.getFont().getFamily();
		lblDieserTextSoll.setFont(new Font(schriftart, Font.BOLD, groesse-1));
		
	}

	public void buttonGroesser_clicked() {
		int groesse = lblDieserTextSoll.getFont().getSize();
		String schriftart = lblDieserTextSoll.getFont().getFamily();
		lblDieserTextSoll.setFont(new Font(schriftart, Font.BOLD, groesse+1));
		
	}

	public void buttonSchriftfarbeschwarz_clicked() {
		this.lblDieserTextSoll.setForeground(Color.BLACK);
		
	}

	public void buttonSchriftfarbeblau_clicked() {
		this.lblDieserTextSoll.setForeground(Color.BLUE);
		
	}

	public void buttonSchriftfarberot_clicked() {
		this.lblDieserTextSoll.setForeground(Color.RED);
		
	}

	public void buttonLoeschen_clicked() {
		this.lblDieserTextSoll.setText("");
		
	}

	public void buttonTexteing_clicked() {
		this.lblDieserTextSoll.setText(this.txtHierBitteText.getText());
		
	}

	public void buttonCourier_clicked() {
		this.lblDieserTextSoll.setFont(new Font("Courier New", Font.BOLD, 12));
		
	}

	public void buttonComic_clicked() {
		this.lblDieserTextSoll.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		
	}

	public void buttonArial_clicked() {
		this.lblDieserTextSoll.setFont(new Font("Arial", Font.BOLD, 12));
		
	}

	public void buttonStandard_clicked() {
		this.contentPane.setBackground(SystemColor.menu);
		
	}

	public void buttonGelb_clicked() {
		this.contentPane.setBackground(Color.YELLOW);
		
	}

	public void buttonBlau_clicked() {
		this.contentPane.setBackground(Color.BLUE);
		
	}

	public void buttonGrn_clicked() {
		this.contentPane.setBackground(Color.GREEN);
		
	}

	public void buttonRot_clicked() {
		this.contentPane.setBackground(Color.RED);
		
	}
}
