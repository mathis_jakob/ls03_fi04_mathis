package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI4 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI4 frame = new GUI4();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI4() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDieserTextSoll = new JLabel("Dieser Text soll ver\u00E4ndert werden");
		lblDieserTextSoll.setBounds(10, 23, 416, 14);
		contentPane.add(lblDieserTextSoll);
		lblDieserTextSoll.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblAufgabe1 = new JLabel("Aufagbe 1: Hintergrundfarbe �ndern");
		lblAufgabe1.setBounds(10, 100, 416, 14);
		contentPane.add(lblAufgabe1);
		
		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(10, 200, 416, 14);
		contentPane.add(lblAufgabe2);
		
		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe �ndern");
		lblAufgabe3.setBounds(10, 300, 416, 14);
		contentPane.add(lblAufgabe3);
		
		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr��e ver�ndern");
		lblAufgabe4.setBounds(10, 400, 416, 14);
		contentPane.add(lblAufgabe4);
		
		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setBounds(10, 500, 416, 14);
		contentPane.add(lblAufgabe5);
		
		JLabel lblAufgabe6 = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabe6.setBounds(10, 600, 416, 14);
		contentPane.add(lblAufgabe6);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRot_Clicked();
			}
		});
		btnRot.setBounds(10, 125, 89, 23);
		contentPane.add(btnRot);
		
		JButton btnGruen = new JButton("Gr�n");
		btnGruen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGruen_Clicked();
			}
		});
		btnGruen.setBounds(100, 125, 89, 23);
		contentPane.add(btnGruen);
		
		

	}

	protected void btnGruen_Clicked() {
		contentPane.setBackground(Color.GREEN);
		
	}

	protected void btnRot_Clicked() {
		contentPane.setBackground(Color.RED);
		
	}
}
