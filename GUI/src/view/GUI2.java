package view;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI2 {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI2 window = new GUI2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 317, 527);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDieserTextSoll = new JLabel("Dieser Text soll ver\u00E4ndert werden");
		lblDieserTextSoll.setBounds(10, 34, 283, 14);
		frame.getContentPane().add(lblDieserTextSoll);
		lblDieserTextSoll.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblAufgabe1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabe1.setBounds(10, 100, 248, 14);
		frame.getContentPane().add(lblAufgabe1);
		
		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(10, 186, 251, 21);
		frame.getContentPane().add(lblAufgabe2);
		
		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setBounds(10, 266, 248, 21);
		frame.getContentPane().add(lblAufgabe3);
		
		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblAufgabe4.setBounds(10, 319, 248, 21);
		frame.getContentPane().add(lblAufgabe4);
		
		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setBounds(10, 373, 251, 21);
		frame.getContentPane().add(lblAufgabe5);
		
		JLabel lblAufgabe6 = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabe6.setBounds(10, 430, 251, 14);
		frame.getContentPane().add(lblAufgabe6);
		
		textField = new JTextField();
		textField.setBounds(10, 235, 283, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Rot");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRot_Clicked();
			}
		});
		btnNewButton.setBounds(10, 125, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Gr\u00FCn");
		btnNewButton_1.setBounds(109, 125, 89, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Blau");
		btnNewButton_2.setBounds(208, 125, 89, 23);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Gelb");
		btnNewButton_3.setBounds(10, 152, 89, 23);
		frame.getContentPane().add(btnNewButton_3);
	}

	protected void btnRot_Clicked() {
		frame.setBackground(Color.RED);
		
	}
}
