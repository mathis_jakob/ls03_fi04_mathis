package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;
import javax.swing.border.EmptyBorder;
import java.awt.Rectangle;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private AlienDefenceController alienDefenceController;
	private List<Level> arrLevel;
	int meneu = 0;


	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(LevelController lvlControl, LeveldesignWindow leveldesignWindow, AlienDefenceController alienDefenceController, User user, int menue) {
		setBounds(new Rectangle(100, 100, 450, 340));
		setBorder(new EmptyBorder(0, 0, 0, 0));
		
		this.alienDefenceController = alienDefenceController;
		
		arrLevel = this.alienDefenceController.getLevelController().readAllLevels();
		String[] arrLevelNames = getLevelNames(arrLevel);
		

		setBackground(Color.BLACK);
		setForeground(Color.orange);
		this.lvlControl = lvlControl;
		this.leveldesignWindow = leveldesignWindow;
		setLayout(null);

		JPanel pnlButtons = new JPanel();
		pnlButtons.setBounds(0, 267, 450, 106);
		pnlButtons.setBackground(Color.BLACK);
		add(pnlButtons);

		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.setBounds(32, 5, 91, 23);
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		pnlButtons.setLayout(null);
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.setBounds(128, 5, 185, 23);
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);
		

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.setBounds(318, 5, 99, 23);
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);
		
		
		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSpielen_Clicked(alienDefenceController);
			}
		});
		
		btnSpielen.setBounds(32, 33, 385, 23);
		pnlButtons.add(btnSpielen);
		
		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnHighscore_Clicked(alienDefenceController);
			}
		});
		btnHighscore.setBounds(32, 61, 385, 23);
		pnlButtons.add(btnHighscore);

		if (menue == 1) {
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
			btnSpielen.setVisible(true);
			btnHighscore.setVisible(false); 
		}	
		else if (menue == 2) {
			btnNewLevel.setVisible(true);
			btnUpdateLevel.setVisible(true);
			btnDeleteLevel.setVisible(true);
			btnSpielen.setVisible(false);
			btnHighscore.setVisible(false); 
		}
		else {
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
			btnSpielen.setVisible(false);
			btnHighscore.setVisible(true); 
		}
			
		
		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setBounds(0, 0, 450, 22);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		lblLevelauswahl.setForeground(new Color(124, 252, 0));
		lblLevelauswahl.setBackground(Color.BLACK);
		add(lblLevelauswahl);

		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setBounds(0, 22, 450, 245);
		spnLevels.setForeground(Color.BLACK);
		add(spnLevels);

		tblLevels = new JTable();
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
	}

	protected void btnHighscore_Clicked(AlienDefenceController alienDefenceController2) {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		new Highscore(alienDefenceController.getAttemptController(), level_id);
	}

	public void btnSpielen_Clicked(AlienDefenceController alienDefenceController) {
//		int level_id = Integer
//				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
//		Level level = alienDefenceController.getLevelController().readLevel(level_id);
//
//		GameController gameController = alienDefenceController.startGame(level, user);
//		new GameGUI(gameController).start();
//	}
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		Level level = alienDefenceController.getLevelController().readLevel(level_id);

		User user = new User(1, "test", "pass");
		Thread t = new Thread("GameThread") {
			@Override
			public void run() {

				
				GameController gameController = alienDefenceController.startGame(level, user);
				new GameGUI(gameController).start();
			}
		};
		t.start();
		this.leveldesignWindow.dispose();
	}

//		User user = new User(1, "test", "pass");
		//Levelnamen auslesen
//		arrLevel = this.alienDefenceController.getLevelController().readAllLevels();
//		String[] arrLevelNames = getLevelNames(arrLevel);
//		Thread t = new Thread("GameThread") {
//
//			@Override
//			public void run() {
//
//				GameController gameController = alienDefenceController.startGame(arrLevel.get(0), user);
//				new GameGUI(gameController).start();
//			}
//		};
//		t.start();
		
//		Thread t = new Thread("GameThread") {
//			@Override
//			public void run() {
//				int level_id = Integer
//						.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
//				Level level = alienDefenceController.getLevelController().readLevel(level_id);
//
//				GameController gameController = alienDefenceController.startGame(level, user);
//				new GameGUI(gameController).start();
//			}
//		};
//		t.start();
		
//	}



	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}
	
	private String[] getLevelNames(List<Level> arrLevel) {
		String[] arrLevelNames = new String[arrLevel.size()];

		for (int i = 0; i < arrLevel.size(); i++) {
			arrLevelNames[i] = arrLevel.get(i).getName(); // Array aus Arraylist erstellt
		}

		return arrLevelNames;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
		//Levelnamen auslesen
		arrLevel = this.alienDefenceController.getLevelController().readAllLevels();
		String[] arrLevelNames = getLevelNames(arrLevel);
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
	public Rectangle getThisBounds() {
		return getBounds();
	}
	public void setThisBounds(Rectangle bounds) {
		setBounds(bounds);
	}
}
